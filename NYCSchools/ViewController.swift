//
//  ViewController.swift
//  NYCSchools
//
//  Created by __this__ on 8/23/18.
//  Copyright © 2018 __this__. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var serverResponseCategories : NSArray?
    
    var schoolsURL = "https://data.cityofnewyork.us/resource/97mf-9njv.json"
    
    var schoolsDetailsURL = "https://data.cityofnewyork.us/resource/734v-jeq5.json"
    
    var taskSchoolsURLComplete : Bool = false
    
    var taskSchoolsDetailsURLComplete : Bool = false
    
    var schoolsArray = [SchoolModel]()
    
    var schoolDetailsArray = [SchoolModel]()
    
    var mergedArray = [SchoolModel]() {
        didSet{
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        
//        AppService.fetchAppDetails(withURL: self.schoolsURL) { (schools) -> Void in
//            //Get the main thread to update the UI
//            DispatchQueue.main.async {
//                self.schoolsArray = schools
//            }
//        }
        
        // title
        self.title = "NYC High Schools"
        
        self.asyncFunctionSchoolsList()
        self.asyncFunctionSchoolsResultsList()
    }
    
    
    func asyncFunctionSchoolsList() {
        
        AppService.fetchAppDetails(withURL: self.schoolsURL) { (schools) -> Void in
            self.schoolsArray = schools
            
            self.taskSchoolsURLComplete = true
            self.mergeResults()
        }
    }
    
    func asyncFunctionSchoolsResultsList() {
        AppService.fetchAppDetails(withURL: self.schoolsDetailsURL) { (schools) -> Void in
            self.schoolDetailsArray = schools
            
            self.taskSchoolsDetailsURLComplete = true
            self.mergeResults()
        }
    }
    
    func mergeResults(){
        // Why does this function have to be called twice?!
        if self.taskSchoolsURLComplete && self.taskSchoolsDetailsURLComplete {
            //Handle Result
            
            var tmpArray = Array(Dictionary([self.schoolDetailsArray, self.schoolsArray].joined().map { ($0.dbn!, $0)}, uniquingKeysWith: { $1 }).values)
            
            tmpArray = tmpArray.filter { $0.overview_paragraph != nil }
            
            DispatchQueue.main.async {
                self.mergedArray = tmpArray
            }
        }
    }
    
    // tableview datasource and delegate methods
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Categories") as! CategoriesTableViewCell
        
        // customize cell
        let element = self.mergedArray[indexPath.row]
        
        cell.lblSchools.text = element.school_name
        
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor.lightGray
        } else {
            cell.backgroundColor = UIColor.clear
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mergedArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        // push itemsViewController
        let controller = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SchoolDetailsViewController") as! SchoolDetailsViewController
        
        let school = self.mergedArray[indexPath.row]
        
        controller.schoolModel = school
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

