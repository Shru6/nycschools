//
//  CategoriesTableViewCell.swift
//  NYCSchools
//
//  Created by __this__ on 8/23/18.
//  Copyright © 2018 __this__. All rights reserved.
//

import UIKit

class CategoriesTableViewCell: UITableViewCell {

    @IBOutlet weak var lblSchools: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: animated)
    }

}
