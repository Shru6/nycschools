//
//  SchoolDetailsTableViewCell.swift
//  ChefChus
//
//  Created by __this__ on 8/23/18.
//  Copyright © 2018 __this__. All rights reserved.
//

import UIKit

class SchoolDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDetails: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
