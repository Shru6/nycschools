//
//  AppService.swift
//  AppInfo
//
//  Created by Jenifer  on 9/6/17.
//  Copyright © 2017 Jenifer . All rights reserved.
//

import Foundation

struct AppService {
    
    fileprivate static func constructURL(url : String) -> URL? {
        
        let appURLComponent = URLComponents(string: url)
        
        return(appURLComponent?.url)
    }

    static func fetchAppDetails( withURL url : String, _ completion: @escaping ([SchoolModel]) -> Void){
        
        if let _url = constructURL(url : url){
            
            let nwOperator = NetworkOperator.init(url: _url)
            nwOperator.getData({ (jsonDict) -> Void in
                let schoolDetails = parseData(jsonDict)
                completion(schoolDetails)
            })
        }
    }
    
    static func parseData(_ jsonObj: [String:AnyObject]?) -> [SchoolModel] {
        
        var schools = [SchoolModel]()
        
        if let results = jsonObj!["schools"] as? [[String:AnyObject]] {
            
            for item in results {
                
                var schoolModel = SchoolModel()
                
                if let dbn = item["dbn"] as? String {
                    schoolModel.dbn = dbn
                    
                    if let school_name = item["school_name"] as? String {
                        schoolModel.school_name = school_name
                    }
                    
                    if let overview_paragraph = item["overview_paragraph"] as? String {
                        schoolModel.overview_paragraph = overview_paragraph
                    }
                    
                    if let primary_address_line_1 = item["primary_address_line_1"] as? String {
                        schoolModel.primary_address_line_1 = primary_address_line_1
                    }
                    
                    if let city = item["city"] as? String {
                        schoolModel.city = city
                    }
                    
                    if let state_code = item["state_code"] as? String {
                        schoolModel.state_code = state_code
                    }
                    
                    if let zip = item["zip"] as? String {
                        schoolModel.zip = zip
                    }
                    
                    if let phone_number = item["phone_number"] as? String {
                        schoolModel.phone_number = phone_number
                    }
                    
                    if let fax_number = item["fax_number"] as? String {
                        schoolModel.fax_number = fax_number
                    }
                    
                    if let website = item["website"] as? String {
                        schoolModel.website = website
                    }
                    
                    if let school_email = item["school_email"] as? String {
                        schoolModel.school_email = school_email
                    }
                    
                    if let total_students = item["total_students"] as? String {
                        schoolModel.total_students = total_students
                    }
                    
                    if let extracurricular_activities = item["extracurricular_activities"] as? String {
                        schoolModel.extracurricular_activities = extracurricular_activities
                    }
                    
                    if let num_of_sat_test_takers = item["num_of_sat_test_takers"] as? String {
                        schoolModel.num_of_sat_test_takers = num_of_sat_test_takers
                    }
                    
                    if let sat_critical_reading_avg_score = item["sat_critical_reading_avg_score"] as? String {
                        schoolModel.sat_critical_reading_avg_score = sat_critical_reading_avg_score
                    }
                    
                    if let sat_math_avg_score = item["sat_math_avg_score"] as? String {
                        schoolModel.sat_math_avg_score = sat_math_avg_score
                    }
                    
                    if let sat_writing_avg_score = item["sat_writing_avg_score"] as? String {
                        schoolModel.sat_writing_avg_score = sat_writing_avg_score
                    }
                }
                
                schools.append(schoolModel)

            }
        }
        return schools
    }
}
