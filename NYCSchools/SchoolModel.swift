//
//  SchoolsModel.swift
//  NYCSchools
//
//  Created by __this__ on 8/23/18.
//  Copyright © 2018 __this__. All rights reserved.
//

import Foundation

struct SchoolModel {
    var school_name : String?
    var dbn : String?
    
    var overview_paragraph : String?
    
    var primary_address_line_1 : String?
    var city : String?
    var state_code : String?
    var zip : String?
    
    var phone_number : String?
    var fax_number : String?
    
    var website : String?
    
    var school_email : String?
    
    var total_students : String?
    
    var extracurricular_activities : String?
    
    var num_of_sat_test_takers : String?
    var sat_critical_reading_avg_score : String?
    var sat_math_avg_score : String?
    var sat_writing_avg_score : String?

}
