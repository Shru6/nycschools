//
//  NetworkOperator.swift
//  AppInfo
//
//  Created by Jenifer  on 9/6/17.
//  Copyright © 2017 Jenifer . All rights reserved.
//

import Foundation

/*
 Network Operator class downloads the data from a given URL and returns the JSON.
 */

struct NetworkOperator {
    
    let fetchURL: URL?
    
    //Custom Init to accept the URL
    init(url: URL) {
        fetchURL = url
    }
    //Downloads the data
    //Returns the JSON Dictionary
    func getData(_ completion: @escaping ([String:AnyObject]) -> Void) {
        
        //Create configuration object
        let configuration: URLSessionConfiguration = URLSessionConfiguration.default
        
        //Create session object with configuration
        let session: URLSession = URLSession(configuration:configuration)
        
        //Create request object with URL
        let request = URLRequest(url: fetchURL!)
        
        //Create data task
        let task = session.dataTask(with: request, completionHandler: {(data, response, error) in
            do{
                //Get json from Foundation Object
                let dataArray = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [[String:AnyObject]]
                let jsonDict = ["schools" : dataArray] as [String : AnyObject]
                completion(jsonDict)
            }
            catch {
                print("json error: \(error)")
            }

        });
        task.resume()
    }
}

