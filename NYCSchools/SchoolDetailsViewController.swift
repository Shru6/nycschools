//
//  ItemsViewController.swift
//  NYCSchools
//
//  Created by __this__ on 8/23/18.
//  Copyright © 2018 __this__. All rights reserved.
//

import UIKit

class SchoolDetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var schoolModel : SchoolModel?
    
    var detailsString = String() {
        didSet{
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorInset = .zero
        self.tableView.tableFooterView = UIView()

        // title
        self.setupTitle()
        
        // set up labels
        self.setupLabels()
    }
    
    fileprivate func setupTitle() {
        if let model = self.schoolModel  {
            self.title = model.school_name
            let tlabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 40))
            tlabel.text = self.title
            tlabel.textColor = UIColor.black
            tlabel.font = UIFont(name: "Helvetica-Bold", size: 30.0)
            tlabel.backgroundColor = UIColor.clear
            tlabel.adjustsFontSizeToFitWidth = true
            tlabel.textAlignment = .center;
            self.navigationItem.titleView = tlabel
        }
    }
    
    fileprivate func setupLabels() {
        var _details = ""
        
        if let overview = self.schoolModel?.overview_paragraph {
            _details += "Overview:\n\(overview)"
        }
        
        if let primary_address_line_1 = self.schoolModel?.primary_address_line_1,
            let city = self.schoolModel?.city,
            let state_code = self.schoolModel?.state_code,
            let zip = self.schoolModel?.zip {
            _details += "\n\nAddress:\n\(primary_address_line_1)\n\(city) \(state_code) \(zip)"
        }
        
        if let website = self.schoolModel?.website {
            _details += "\n\nWebsite: \(website)"
        }
        
        if let sat_math_avg_score = self.schoolModel?.sat_math_avg_score {
            _details += "\n\nSAT Math Average Score: \(sat_math_avg_score)"
        }
        
        if let sat_writing_avg_score = self.schoolModel?.sat_writing_avg_score {
            _details += "\n\nSAT Writing Average Score: \(sat_writing_avg_score)"
        }
        
        if let sat_critical_reading_avg_score = self.schoolModel?.sat_critical_reading_avg_score {
            _details += "\n\nSAT Critical Reading Average Score: \(sat_critical_reading_avg_score)"
        }
        
        if let num_of_sat_test_takers = self.schoolModel?.num_of_sat_test_takers {
            _details += "\n\nNumber of SAT takers: \(num_of_sat_test_takers)"
        }
        
        self.detailsString = _details
    }
    
    // table view  methods
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolDetailsTableViewCell") as! SchoolDetailsTableViewCell
        
        cell.lblDetails.text = self.detailsString
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
